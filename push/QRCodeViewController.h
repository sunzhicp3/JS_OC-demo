//
//  QRCodeViewController.h
//  push
//
//  Created by 孙智 on 2017/7/26.
//  Copyright © 2017年 孙智. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QRCodeViewController : UIViewController
@property (nonatomic, copy) void (^QRCodeCancleBlock) (QRCodeViewController *);//扫描取消
@property (nonatomic, copy) void (^QRCodeSucessBlock) (QRCodeViewController *,NSString *);//扫描成功结果
@property (nonatomic, copy) void (^QRCodeFailBlock) (QRCodeViewController *);//扫描失败
@end
