//
//  FileHelper.h
//  push
//
//  Created by 孙智 on 2017/7/27.
//  Copyright © 2017年 孙智. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FileHelper : NSObject
// 将文件拷贝到tmp目录
- (NSURL *)fileURLForBuggyWKWebView8:(NSURL *)fileURL;

@end
