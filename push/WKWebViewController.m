//
//  WKWebViewController.m
//  push
//
//  Created by 孙智 on 2017/7/25.
//  Copyright © 2017年 孙智. All rights reserved.
//

#import "WKWebViewController.h"
#import <WebKit/WebKit.h>

@interface WKWebViewController ()<WKUIDelegate,WKNavigationDelegate,WKScriptMessageHandler>
@property (nonatomic, strong) WKWebView *webView;
@property (nonatomic, strong) FileHelper *fileHelper;
@end

@implementation WKWebViewController
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    // addScriptMessageHandler 很容易导致循环引用
    // 控制器 强引用了WKWebView,WKWebView copy(强引用了）configuration， configuration copy （强引用了）userContentController
    // userContentController 强引用了 self （控制器）
    [self.webView.configuration.userContentController addScriptMessageHandler:self name:@"ScanAction"];
    [self.webView.configuration.userContentController addScriptMessageHandler:self name:@"Location"];
    [self.webView.configuration.userContentController addScriptMessageHandler:self name:@"Share"];
    [self.webView.configuration.userContentController addScriptMessageHandler:self name:@"Pay"];
}
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    // 因此这里要记得移除handlers
    [self.webView.configuration.userContentController removeScriptMessageHandlerForName:@"ScanAction"];
    [self.webView.configuration.userContentController removeScriptMessageHandlerForName:@"Location"];
    [self.webView.configuration.userContentController removeScriptMessageHandlerForName:@"Share"];
    [self.webView.configuration.userContentController removeScriptMessageHandlerForName:@"Pay"];
    [self.webView.configuration.userContentController removeScriptMessageHandlerForName:@"Shake"];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = _tit;
    self.view.backgroundColor = [UIColor whiteColor];
    [self initWKWebView];
}
- (void)initWKWebView {
    /*
     以前用UIWebView方式加载网页时，CSS样式可以自动的被应用到html中，但是在新的WKWebView中，工程中的CSS样式是不会被自动加载到网页中的，那么我们就必须手动添加CSS文件
     那么如何加载CSS样式呢，我们可以用脚本挂载CSS样式，然后将 脚本注入 到网页中，这样就能够实现CSS样式的加载了
     */
    /*
    WKWebView是不允许通过loadRequest的方法来加载本地根目录的HTML文件。
    而在iOS9的SDK中加入了以下方法来加载本地的HTML文件：
    [WKWebView loadFileURL:allowingReadAccessToURL:]
    但是在iOS9以下的版本是没提供这个便利的方法的,解决方法如下
    */

    self.fileHelper = [[FileHelper alloc]init];
    
    // 运行时加载JS代码
    NSString *js = @"I am JS Code";
    //初始化WKUserScript对象
    //WKUserScriptInjectionTimeAtDocumentEnd为网页加载完成时注入
    WKUserScript *script = [[WKUserScript alloc] initWithSource:js injectionTime:WKUserScriptInjectionTimeAtDocumentEnd forMainFrameOnly:YES];
    // 配置环境
    WKWebViewConfiguration *configuration = [[WKWebViewConfiguration alloc]init];
    [configuration.userContentController addUserScript:script];
    
    // 创建设置对象
    WKPreferences *preference = [[WKPreferences alloc]init];
    // 设置字体大小(最小的字体大小)
    preference.minimumFontSize = 30;
    // 设置偏好设置对象
    configuration.preferences = preference;

    WKUserContentController *userContentController = [[WKUserContentController alloc]init];
    configuration.userContentController = userContentController;
    
    
    self.webView = [[WKWebView alloc]initWithFrame:self.view.frame configuration:configuration];
    NSString *path = [[NSBundle mainBundle] pathForResource:@"wkwebview" ofType:@"html"];
    NSString *html = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:nil];
    [self.webView loadHTMLString:html baseURL:nil];
//    if(path){
//        if ([[UIDevice currentDevice].systemVersion floatValue] >= 9.0) {
//            // iOS9. One year later things are OK.
//            NSURL *fileURL = [NSURL fileURLWithPath:path];
//            [self.webView loadFileURL:fileURL allowingReadAccessToURL:fileURL];
//            
//        } else {
//            // iOS8. Things can be workaround-ed
//            //   Brave people can do just this
//            //   fileURL = try! pathForBuggyWKWebView8(fileURL)
//            //   webView.loadRequest(NSURLRequest(URL: fileURL))
//            
//            NSURL *fileURL = [self.fileHelper fileURLForBuggyWKWebView8:[NSURL fileURLWithPath:path]];
//            NSURLRequest *request = [NSURLRequest requestWithURL:fileURL];
//            [self.webView loadRequest:request];
//        }
//    }
    
    self.webView.UIDelegate = self;
    self.webView.navigationDelegate = self;
    [self.view addSubview:self.webView];
    /*
     WKNavigationDelegate主要处理一些跳转、加载处理操作，WKUIDelegate主要处理JS脚本，确认框，警告框等
     
     WKWebview提供了API实现js交互 不需要借助JavaScriptCore或者webJavaScriptBridge。使用WKUserContentController实现js native交互
     */
}
#pragma mark -------WKNavigationDelegate-----------
// 页面开始加载时调用
- (void)webView:(WKWebView *)webView didStartProvisionalNavigation:(WKNavigation *)navigation{
    NSLog(@"didStartProvisionalNavigation");
}
// 当内容开始返回时调用
- (void)webView:(WKWebView *)webView didCommitNavigation:(WKNavigation *)navigation{
    NSLog(@"didCommitNavigation");
}
// 页面加载完成之后调用
- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation{
    NSLog(@"didFinishNavigation");
}
// 页面加载失败时调用
- (void)webView:(WKWebView *)webView didFailProvisionalNavigation:(WKNavigation *)navigation{
    NSLog(@"didFailProvisionalNavigation");
}
// 接收到服务器跳转请求之后调用
- (void)webView:(WKWebView *)webView didReceiveServerRedirectForProvisionalNavigation:(WKNavigation *)navigation{
    
}
// 在收到响应后，决定是否跳转
- (void)webView:(WKWebView *)webView decidePolicyForNavigationResponse:(WKNavigationResponse *)navigationResponse decisionHandler:(void (^)(WKNavigationResponsePolicy))decisionHandler{
    
    NSLog(@"%@",navigationResponse.response.URL.absoluteString);
    //允许跳转
    decisionHandler(WKNavigationResponsePolicyAllow);
    //不允许跳转
    //decisionHandler(WKNavigationResponsePolicyCancel);
}
// 在发送请求之前，决定是否跳转
- (void)webView:(WKWebView *)webView decidePolicyForNavigationAction:(WKNavigationAction *)navigationAction decisionHandler:(void (^)(WKNavigationActionPolicy))decisionHandler{
    
    NSLog(@"%@",navigationAction.request.URL.absoluteString);
    //允许跳转
    decisionHandler(WKNavigationActionPolicyAllow);
    //不允许跳转
    //decisionHandler(WKNavigationActionPolicyCancel);
}
#pragma mark --------WKUIDelegate--------------
// 创建一个新的WebView
- (WKWebView *)webView:(WKWebView *)webView createWebViewWithConfiguration:(WKWebViewConfiguration *)configuration forNavigationAction:(WKNavigationAction *)navigationAction windowFeatures:(WKWindowFeatures *)windowFeatures{
    return [[WKWebView alloc]init];
}
// 输入框
- (void)webView:(WKWebView *)webView runJavaScriptTextInputPanelWithPrompt:(NSString *)prompt defaultText:(nullable NSString *)defaultText initiatedByFrame:(WKFrameInfo *)frame completionHandler:(void (^)(NSString * __nullable result))completionHandler{
    completionHandler(@"http");
}
// 确认框
- (void)webView:(WKWebView *)webView runJavaScriptConfirmPanelWithMessage:(NSString *)message initiatedByFrame:(WKFrameInfo *)frame completionHandler:(void (^)(BOOL result))completionHandler{
    completionHandler(YES);
   
}
// 警告框
- (void)webView:(WKWebView *)webView runJavaScriptAlertPanelWithMessage:(NSString *)message initiatedByFrame:(WKFrameInfo *)frame completionHandler:(void (^)(void))completionHandler{
    NSLog(@"%@",message);
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"提示" message:message preferredStyle:UIAlertControllerStyleAlert];
    [alert addAction:[UIAlertAction actionWithTitle:@"知道了" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        completionHandler();
    }]];
    
    [self presentViewController:alert animated:YES completion:nil];
}
#pragma mark - WKScriptMessageHandler
- (void)userContentController:(WKUserContentController *)userContentController didReceiveScriptMessage:(WKScriptMessage *)message
{
    //    message.body  --  Allowed types are NSNumber, NSString, NSDate, NSArray,NSDictionary, and NSNull.
    NSLog(@"body:%@,name:%@",message.body,message.name);
    if ([message.name isEqualToString:@"ScanAction"]) {
        [self scan];
    } else if ([message.name isEqualToString:@"Location"]) {
        [self getLocation];
    } else if ([message.name isEqualToString:@"Share"]) {
        [self shareWithParams:message.body];
    } else if ([message.name isEqualToString:@"Pay"]) {
        [self payWithParams:message.body];
    }
}
#pragma mark ios action
- (void)scan {
    [self goToQRCodeVC];
}
- (void)getLocation {
    NSString *str = [NSString stringWithFormat:@"当前位置的经纬度：%f,%f , 当前城市：%@",[AppDelegate shareAppDelegate].lon,[AppDelegate shareAppDelegate].lat,[AppDelegate shareAppDelegate].cityName];
    NSString *jsStr = [NSString stringWithFormat:@"setLocation('%@')",str];
    // [[JSContext currentContext] evaluateScript:jsStr];
    [self.webView evaluateJavaScript:jsStr completionHandler:^(id _Nullable result, NSError * _Nullable error) {
        NSLog(@"%@----%@",result, error);
    }];
}
- (void)shareWithParams:(NSDictionary *)params {
    if (![params isKindOfClass:[NSDictionary class]]) {
        return;
    }
    
    NSString *title = [params objectForKey:@"title"];
    NSString *content = [params objectForKey:@"content"];
    NSString *url = [params objectForKey:@"url"];
    NSString *scene = [params objectForKey:@"scene"];
    // 在这里执行分享的操作
    [self shareToWXWithTitle:title content:content urlStr:url scene:scene];
    
    // 将分享结果返回给js
//    NSString *jsStr = [NSString stringWithFormat:@"shareResult('%@','%@','%@')",title,content,url];
//    [self.webView evaluateJavaScript:jsStr completionHandler:^(id _Nullable result, NSError * _Nullable error) {
//        NSLog(@"%@----%@",result, error);
//    }];

}
- (void)payWithParams:(NSDictionary *)params {
    if (![params isKindOfClass:[NSDictionary class]]) {
        return;
    }
    NSString *orderNo = [params objectForKey:@"order_no"];
    long long amount = [[params objectForKey:@"amount"] longLongValue];
    NSString *subject = [params objectForKey:@"subject"];
    NSString *channel = [params objectForKey:@"channel"];
    NSLog(@"%@---%lld---%@---%@",orderNo,amount,subject,channel);
    
    // 支付操作
    
    // 将支付结果返回给js
    NSString *jsStr = [NSString stringWithFormat:@"payResult('%@')",@"支付成功"];
    [self.webView evaluateJavaScript:jsStr completionHandler:^(id _Nullable result, NSError * _Nullable error) {
        NSLog(@"%@----%@",result, error);
    }];
}
- (void)goToQRCodeVC {
    QRCodeViewController *qrCodeVC = [[QRCodeViewController alloc]init];
    [self presentViewController:qrCodeVC animated:YES completion:nil];
    
    __weak typeof(self) weakSelf = self;
    
    qrCodeVC.QRCodeSucessBlock = ^(QRCodeViewController *aqrvc,NSString *qrString) {
        [aqrvc dismissViewControllerAnimated:YES completion:nil];
        NSLog(@"扫描成功结果：%@",qrString);
        // 扫描成功后可以在此实现自己的逻辑
        UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"扫描到的信息" message:qrString delegate:nil cancelButtonTitle:@"确定" otherButtonTitles: nil];
        [alert show];
        
        NSString *jsStr = [NSString stringWithFormat:@"getQRCode('%@')",qrString];
        [weakSelf.webView evaluateJavaScript:jsStr completionHandler:^(id _Nullable result, NSError * _Nullable error) {
            NSLog(@"result:%@,error:%@",result,error);
        }];
    };
    qrCodeVC.QRCodeFailBlock = ^(QRCodeViewController *aqrvc) {
        [aqrvc dismissViewControllerAnimated:YES completion:nil];
        UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"提示" message:@"扫描失败" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles: nil];
        [alert show];
    };
    qrCodeVC.QRCodeCancleBlock = ^(QRCodeViewController *aqrvc) {
        [aqrvc dismissViewControllerAnimated:YES completion:nil];
        UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"提示" message:@"您取消了扫描" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles: nil];
        [alert show];
    };
}
- (void)shareToWXWithTitle:(NSString *)title content:(NSString *)content urlStr:(NSString *)urlStr scene:(NSString *)scene {
    WXMediaMessage *message = [WXMediaMessage message];
    message.title = title;
    message.description = content;
    [message setThumbImage:[UIImage imageNamed:@"AppIcon"]];
    
    WXWebpageObject *webObject = [WXWebpageObject object];
    webObject.webpageUrl = @"http://itellu.cnsaas.com";
    message.mediaObject = webObject;
    
    SendMessageToWXReq *req = [[SendMessageToWXReq alloc]init];
    req.bText = NO;
    req.message = message;
    if ([scene isEqualToString:@"0"]) {
        req.scene = WXSceneSession;
    }else if ([scene isEqualToString:@"1"]) {
        req.scene = WXSceneTimeline;
    }
    
    [WXApi sendReq:req];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
