//
//  main.m
//  push
//
//  Created by 孙智 on 2017/7/19.
//  Copyright © 2017年 孙智. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
