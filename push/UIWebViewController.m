//
//  webViewController.m
//  push
//
//  Created by 孙智 on 2017/7/25.
//  Copyright © 2017年 孙智. All rights reserved.
//

#import "UIWebViewController.h"
#import <JavaScriptCore/JavaScriptCore.h>

@interface UIWebViewController ()<UIWebViewDelegate>
@property (nonatomic, strong) UIWebView *webView;
@end

@implementation UIWebViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.d
    self.title = _tit;
    self.view.backgroundColor = [UIColor whiteColor];
    [self initWebView];
}
- (void)initWebView {
    self.webView = [[UIWebView alloc]initWithFrame:self.view.frame];
    self.webView.delegate = self;
    NSURL *htmlURL = [[NSBundle mainBundle] URLForResource:@"test.html" withExtension:nil];
    NSURLRequest *request = [NSURLRequest requestWithURL:htmlURL];
    
    self.webView.backgroundColor = [UIColor clearColor];
    // UIWebView 滚动的比较慢，这里设置为正常速度
    self.webView.scrollView.decelerationRate = UIScrollViewDecelerationRateNormal;
    [self.webView loadRequest:request];
    [self.view addSubview:_webView];
}
#pragma 注册api函数
- (void)setJSInterface {
    JSContext *context = [self.webView valueForKeyPath:@"documentView.webView.mainFrame.javaScriptContext"];
    __weak typeof(self) weakSelf = self;
    
    // 注册名为scan的api方法
    context[@"scan"] = ^() {
        [weakSelf goToQRCodeVC];
    };
    
    // 注册名为share的api方法
    context[@"share"] = ^() {
        // 获取参数
        NSArray *args = [JSContext currentArguments];
        NSLog(@"args:%@",args);
        NSString *title = [args[0] toString];
        NSString *content = [args[1] toString];
        NSString *url = [args[2] toString];
        NSString *scene = [args[3] toString];
        
        // 在这里执行分享的操作
        [weakSelf shareToWXWithTitle:title content:content urlStr:url scene:scene];
        // 将分享结果返回给js
//        NSString *jsStr = [NSString stringWithFormat:@"shareResult('%@','%@','%@')",title,content,url];
//        // [[JSContext currentContext] evaluateScript:jsStr];
//        NSString *s = [weakSelf.webView stringByEvaluatingJavaScriptFromString:jsStr];
//        NSLog(@"s:%@",s);
    };
    // 注册名为getLocation的api方法
    context[@"getLocation"] = ^() {
        [weakSelf getLocation];
    };
}
#pragma mark - UIWebViewDelegate
- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    NSLog(@"webViewDidFinishLoad");
    // webview加载完毕后设置js接口
    [self setJSInterface];
}
#pragma mark custonAction
- (void)getLocation {
    NSString *str = [NSString stringWithFormat:@"当前位置的经纬度：%f,%f, 当前城市：%@",[AppDelegate shareAppDelegate].lon,[AppDelegate shareAppDelegate].lat,[AppDelegate shareAppDelegate].cityName];
    NSString *jsStr = [NSString stringWithFormat:@"setLocation('%@')",str];
    // [[JSContext currentContext] evaluateScript:jsStr];
    [self.webView stringByEvaluatingJavaScriptFromString:jsStr];
}
- (void)goToQRCodeVC {
    QRCodeViewController *qrCodeVC = [[QRCodeViewController alloc]init];
    [self presentViewController:qrCodeVC animated:YES completion:nil];
    
    __weak typeof(self) weakSelf = self;
    
    qrCodeVC.QRCodeSucessBlock = ^(QRCodeViewController *aqrvc,NSString *qrString) {
        [aqrvc dismissViewControllerAnimated:YES completion:nil];
        NSLog(@"扫描成功结果：%@",qrString);
        // 扫描成功后可以在此实现自己的逻辑
        UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"扫描到的信息" message:qrString delegate:nil cancelButtonTitle:@"确定" otherButtonTitles: nil];
        [alert show];
        
        NSString *jsStr = [NSString stringWithFormat:@"getQRCode('%@')",qrString];
        [weakSelf.webView stringByEvaluatingJavaScriptFromString:jsStr];
    };
    qrCodeVC.QRCodeFailBlock = ^(QRCodeViewController *aqrvc) {
        [aqrvc dismissViewControllerAnimated:YES completion:nil];
        UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"提示" message:@"扫描失败" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles: nil];
        [alert show];
    };
    qrCodeVC.QRCodeCancleBlock = ^(QRCodeViewController *aqrvc) {
        [aqrvc dismissViewControllerAnimated:YES completion:nil];
        UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"提示" message:@"您取消了扫描" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles: nil];
        [alert show];
    };
}
- (void)shareToWXWithTitle:(NSString *)title content:(NSString *)content urlStr:(NSString *)urlStr scene:(NSString *)scene {
    WXMediaMessage *message = [WXMediaMessage message];
    message.title = title;
    message.description = content;
    [message setThumbImage:[UIImage imageNamed:@"AppIcon"]];
    
    WXWebpageObject *webObject = [WXWebpageObject object];
    webObject.webpageUrl = @"http://itellu.cnsaas.com";
    message.mediaObject = webObject;
    
    SendMessageToWXReq *req = [[SendMessageToWXReq alloc]init];
    req.bText = NO;
    req.message = message;
    if ([scene isEqualToString:@"0"]) {
        req.scene = WXSceneSession;
    }else if ([scene isEqualToString:@"1"]) {
        req.scene = WXSceneTimeline;
    }
    
    [WXApi sendReq:req];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
