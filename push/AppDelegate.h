//
//  AppDelegate.h
//  push
//
//  Created by 孙智 on 2017/7/19.
//  Copyright © 2017年 孙智. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RootViewController.h"


@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) RootViewController *rootController;
@property (strong, nonatomic) CLLocationManager *locationManager;

@property (nonatomic, assign) double lon;//经度
@property (nonatomic, assign) double lat;//纬度
@property (nonatomic, strong) NSString *cityName;

+ (AppDelegate *)shareAppDelegate;

@end

