//
//  RootViewController.m
//  push
//
//  Created by 孙智 on 2017/7/21.
//  Copyright © 2017年 孙智. All rights reserved.
//

#import "RootViewController.h"
#import "MainViewController.h"
#import "PersonalViewController.h"

@interface RootViewController ()

@end

@implementation RootViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setupItem];
    [self addChildVCs];
}
- (void)setupItem {
    // 1. UIControlStateNormal 状态下的属性
    NSMutableDictionary *normalAttr = [NSMutableDictionary dictionary];
    //文字属性
    //normalAttr[NSForegroundColorAttributeName] = [UIColor grayColor];
    //文字大小
    normalAttr[NSFontAttributeName] = [UIFont systemFontOfSize:12];
    
    // 2. UIControlStateSelected 状态下的属性
    NSMutableDictionary *selectAttr = [NSMutableDictionary dictionary];
    
    //文字颜色
    selectAttr[NSForegroundColorAttributeName] = [UIColor colorWithRed:127 / 235.0f green:14 / 255.0f blue:36 / 255.0f alpha:1];
    //selectAttr[NSForegroundColorAttributeName] = [UIColor redColor];
    
    // 统一给所有的UITabBarItem设置文字属性
    // 只有后面带有UI_APPEARANCE_SELECTOR的属性或方法, 才可以通过appearance对象来统一设置
    UITabBarItem *item = [UITabBarItem appearance];
    [item setTitleTextAttributes:normalAttr forState:UIControlStateNormal];
    [item setTitleTextAttributes:selectAttr forState:UIControlStateSelected];
}
- (void)addChildVCs {
    [self setupChildVC:[[MainViewController alloc]init] title:@"首页" image:@"tab1" selectedImage:@"tab1_s"];
    [self setupChildVC:[[PersonalViewController alloc]init] title:@"个人" image:@"tab2" selectedImage:@"tab2_s"];
    
}
- (void)setupChildVC:(UIViewController *)vc title:(NSString *)title image:(NSString *)imageName selectedImage:(NSString *)selectedImageName
{
    // 包装一个导航控制器
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:vc];
    [self addChildViewController:nav];
    
    // 设置子控制器的tabBarItem
    UITabBarItem *tabBarItem = [[UITabBarItem alloc]initWithTitle:title image:[UIImage imageNamed:imageName] selectedImage:[UIImage imageNamed:selectedImageName]];
    nav.tabBarItem = tabBarItem;
    
    //设置UITabBarItem图片不渲染
    UIImage *selectImage = [[UIImage imageNamed:selectedImageName] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    nav.tabBarItem.selectedImage = selectImage;
    
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
