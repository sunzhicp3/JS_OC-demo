//
//  ViewController.m
//  push
//
//  Created by 孙智 on 2017/7/19.
//  Copyright © 2017年 孙智. All rights reserved.
//

#import "MainViewController.h"
#import "UIWebViewController.h"
#import "WKWebViewController.h"

@interface MainViewController ()<UITableViewDataSource,UITableViewDelegate>
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSArray *array;
@end

@implementation MainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    self.title = @"首页";
    _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, kNavgationHeight + kStatusHeight, kWidth, kHeight - kStatusHeight - kNavgationHeight - kTabBarHeight) style:UITableViewStylePlain];
    _tableView.dataSource = self;
    _tableView.delegate = self;
    _tableView.tableFooterView = [[UIView alloc]init];
    self.automaticallyAdjustsScrollViewInsets = false;
    [self.view addSubview:_tableView];
    _array = [[NSArray alloc]initWithObjects:@"UIWebView",@"WKWebView", nil];
}
#pragma mark UITableView
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSInteger row = 0;
    switch (section) {
        case 0:
            row = 1;
            break;
        case 1:
            row = _array.count;
            break;
        default:
            break;
    }
    return row;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    CGFloat height = 0;
    switch (indexPath.section) {
        case 0:
            height = 260.f;
            break;
        case 1:
            height = 60.f;
            break;
            
        default:
            break;
    }
    return height;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
    
    switch (indexPath.section) {
        case 0:
        {
            cell.textLabel.text = @"oc和js交互：常用的两种方式：\n 1：通过拦截URL（第三方库JavaScriptBrige）；\n 2：ios7新增的JavaScriptCore库 (建议使用) \n\n ios加载url的两种控件：UIWebView、WKWebView（ios8）\n\n本demo是使用JavaScriptCore \n\n";
            cell.textLabel.lineBreakMode = NSLineBreakByWordWrapping;
            cell.textLabel.numberOfLines = 0;
            cell.textLabel.textColor = [UIColor redColor];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
            break;
        case 1:
        {
            cell.textLabel.text = _array[indexPath.row];
        }
            break;
            
        default:
            break;
    }
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    switch (indexPath.section) {
        case 0:
            
            break;
        case 1:
        {
            switch (indexPath.row) {
                case 0:
                {
                    UIWebViewController *UIWebViewVC = [[UIWebViewController alloc]init];
                    UIWebViewVC.tit = cell.textLabel.text;
                    UIWebViewVC.hidesBottomBarWhenPushed = YES;
                    [self.navigationController pushViewController:UIWebViewVC animated:YES];
                }
                    
                    break;
                case 1:
                {
                    WKWebViewController *WKWebViewVC = [[WKWebViewController alloc]init];
                    WKWebViewVC.tit = cell.textLabel.text;
                    WKWebViewVC.hidesBottomBarWhenPushed = YES;
                    [self.navigationController pushViewController:WKWebViewVC animated:YES];
                }
                    
                    break;
                    
                default:
                    break;
            }

        }
            
            break;
            
        default:
            break;
    }
    }

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
